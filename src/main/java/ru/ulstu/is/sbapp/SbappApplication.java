package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbappApplication.class, args);
	}

	@GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
	}
	
	@GetMapping("/plus")
    public int plus(@RequestParam(value="num1", defaultValue = "5") int num1,  @RequestParam("num2") int num2) {
        return num1+num2;
	}
	
	@GetMapping("/minus")
    public int minus(@RequestParam("num1") int num1,  @RequestParam("num2") int num2) {
        return num1-num2;
	}
	
	@GetMapping("/umnoj")
    public int umnoj(@RequestParam("num1") int num1,  @RequestParam("num2") int num2) {
        return num1*num2;
	}
	
	@GetMapping("/div")
    public int div(@RequestParam("num1") int num1,  @RequestParam("num2") int num2) {
        return num1/num2;
	}
}